import json


def hello(event, context):
    body = {
        "message": "please work somehow",
        "input": event,
    }

    return {"body": json.dumps(body)}
